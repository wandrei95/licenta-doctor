package com.wegroszta.andrei.licenta.doctor.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.wegroszta.andrei.licenta.doctor.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientStatisticsActivity extends AppCompatActivity implements OptionsView.OnOptionClickedListener {
    public static final String PATIENT_ID_KEY = "patient_id";

    @BindView(R.id.options_view)
    OptionsView optionsView;

    private String patientId;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurements);

        ButterKnife.bind(this);

        extractPatientIdFromIntent();

        optionsView.setOnOptionClickedListener(this);
    }

    private void extractPatientIdFromIntent() {
        Intent intent = getIntent();
        patientId = intent.getStringExtra(PATIENT_ID_KEY);
    }

    @Override
    public void onOptionClicked(final OptionsView.Option option) {
        switch (option) {
            case TEMPERATURE:
                goToTemperatureStatistics();
                break;
            case HEART_RATE:
                goToHeartRateStatistics();
                break;
        }
    }

    private void goToTemperatureStatistics() {
        Intent intent = new Intent(this, TemperatureStatisticsActivity.class);
        intent.putExtra(TemperatureStatisticsActivity.PATIENT_ID_KEY, patientId);
        startActivity(intent);
    }

    private void goToHeartRateStatistics() {
        Intent intent = new Intent(this, HeartRateStatisticsActivity.class);
        intent.putExtra(HeartRateStatisticsActivity.PATIENT_ID_KEY, patientId);
        startActivity(intent);
    }
}

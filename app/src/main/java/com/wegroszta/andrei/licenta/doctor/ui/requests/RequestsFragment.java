package com.wegroszta.andrei.licenta.doctor.ui.requests;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;
import com.wegroszta.andrei.licenta.doctor.ui.common.PatientDetailsActivity;
import com.wegroszta.andrei.licenta.doctor.ui.util.ViewModelFactory;
import com.wegroszta.andrei.licenta.doctor.viewmodels.requests.CollaborationRequestsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

public class RequestsFragment extends Fragment implements RequestsRecycleViewAdapter.OnRequestClickListener, SearchView.OnQueryTextListener {
    private static final int ACCEPT_PATIENT_REQUEST_CODE = 123;

    @BindView(R.id.rv_requests)
    RecyclerView rvRequests;
    @BindView(R.id.tv_no_requests)
    TextView tvNoRequests;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.loading)
    ProgressBar loading;

    private RequestsRecycleViewAdapter adapter;
    private CollaborationRequestsViewModel collaborationRequestsViewModel;
    private Unbinder unbinder;

    public RequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_requests, container, false);

        unbinder = ButterKnife.bind(this, view);

        setupCollaborationRequestsViewModel();

        setupRequestsRecyclerView();
        setupSearchView();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRequests();
    }

    private void setupCollaborationRequestsViewModel() {
        collaborationRequestsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(CollaborationRequestsViewModel.class);
        collaborationRequestsViewModel.getGetRequestsResponse().observe(this,
                this::processGetRequestsResponse);
    }

    private void processGetRequestsResponse(final Response<List<CollaborationRequest>> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onRequestsLoaded(response.data);
                break;
            case COMPLETED:
                onLoadRequestsEnd();
                break;
            case FAILURE:
                onLoadRequestsFail(response.error);
                break;
        }
    }

    private void setupRequestsRecyclerView() {
        adapter = new RequestsRecycleViewAdapter();
        adapter.setOnRequestClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvRequests.setLayoutManager(layoutManager);
        rvRequests.setAdapter(adapter);
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(this);
    }

    private void loadRequests() {
        tvNoRequests.setVisibility(View.GONE);
        rvRequests.setVisibility(View.GONE);

        collaborationRequestsViewModel.getRequests();
    }

    private void onRequestsLoaded(final List<CollaborationRequest> collaborationRequests) {
        if (collaborationRequests.size() > 0) {
            tvNoRequests.setVisibility(View.GONE);
            rvRequests.setVisibility(View.VISIBLE);
            adapter.setRequests(collaborationRequests);
        } else {
            tvNoRequests.setVisibility(View.VISIBLE);
        }
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void onLoadRequestsFail(final Throwable cause) {
        cause.printStackTrace();
        hideLoadingState();
        tvNoRequests.setVisibility(View.VISIBLE);
        Toast.makeText(getActivity(), R.string.load_requests_error_msg, Toast.LENGTH_LONG).show();
    }

    private void onLoadRequestsEnd() {
        hideLoadingState();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onRequestClicked(final CollaborationRequest request) {
        Intent intent = new Intent(getActivity(), PatientDetailsActivity.class);
        intent.putExtra(PatientDetailsActivity.COLLABORATION_REQUEST_KEY, request);
        startActivityForResult(intent, ACCEPT_PATIENT_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCEPT_PATIENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String collaborationRequestId = data.getStringExtra(PatientDetailsActivity.PATIENT_ID);
                adapter.removeCollaborationRequest(collaborationRequestId);
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        adapter.filter(newText);
        return false;
    }
}
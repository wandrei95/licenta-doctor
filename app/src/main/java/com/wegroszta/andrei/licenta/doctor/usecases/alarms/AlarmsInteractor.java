package com.wegroszta.andrei.licenta.doctor.usecases.alarms;

import com.wegroszta.andrei.licenta.doctor.entities.Alarm;

import java.util.List;

import io.reactivex.Observable;

public class AlarmsInteractor {
    private final AlarmsStorage alarmsStorage;

    public AlarmsInteractor(final AlarmsStorage alarmsStorage) {
        this.alarmsStorage = alarmsStorage;
    }

    public Observable<List<Alarm>> getAlarms(final String doctorId) {
        return alarmsStorage.getAlarms(doctorId);
    }

    public Observable<Alarm> removeAlarm(final String doctorId, final Alarm alarm) {
        return alarmsStorage.removeAlarm(doctorId, alarm);
    }
}

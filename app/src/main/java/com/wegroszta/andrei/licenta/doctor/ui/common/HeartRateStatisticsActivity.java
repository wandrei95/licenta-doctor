package com.wegroszta.andrei.licenta.doctor.ui.common;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.HeartRate;
import com.wegroszta.andrei.licenta.doctor.ui.util.ViewModelFactory;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;
import com.wegroszta.andrei.licenta.doctor.viewmodels.statistics.FetchHeartRateStatisticsViewModel;

import java.util.List;

public class HeartRateStatisticsActivity extends StatisticsActivity {
    public static final String PATIENT_ID_KEY = "patient_id";

    private FetchHeartRateStatisticsViewModel fetchHeartRateStatisticsViewModel;
    private String patientId;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupFetchHeartRateStatisticsViewModel();

        extractPatientIdFromIntent();

        getData();
    }

    private void extractPatientIdFromIntent() {
        Intent intent = getIntent();
        patientId = intent.getStringExtra(PATIENT_ID_KEY);
    }

    private void setupFetchHeartRateStatisticsViewModel() {
        fetchHeartRateStatisticsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(FetchHeartRateStatisticsViewModel.class);
        fetchHeartRateStatisticsViewModel.getFetchHeartRateResponse()
                .observe(this, this::processHeartRateResponse);
    }

    private void processHeartRateResponse(final Response<List<HeartRate>> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                showGraph(response.data);
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.error);
                break;
        }
    }

    @Override
    protected String getGraphTitle() {
        return getString(R.string.heart_rate);
    }

    private void getData() {
        fetchHeartRateStatisticsViewModel.getHeartRates(patientId);
    }

}
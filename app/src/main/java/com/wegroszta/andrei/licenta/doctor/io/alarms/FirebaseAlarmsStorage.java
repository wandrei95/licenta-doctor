package com.wegroszta.andrei.licenta.doctor.io.alarms;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.doctor.entities.Alarm;
import com.wegroszta.andrei.licenta.doctor.usecases.alarms.AlarmsStorage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseAlarmsStorage implements AlarmsStorage {
    private final DatabaseReference databaseReference;

    public FirebaseAlarmsStorage(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }

    @Override
    public Observable<List<Alarm>> getAlarms(final String doctorId) {
        return Observable.create(source -> fetchAlarms(doctorId, source));
    }

    @Override
    public Observable<Alarm> removeAlarm(final String doctorId, final Alarm alarm) {
        return Observable.create(source -> removeAlarm(doctorId, alarm, source));
    }

    private void fetchAlarms(final String doctorId, final ObservableEmitter<List<Alarm>> source) {
        DatabaseReference databaseReference = getAlarmsDatabaseReference(doctorId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    @NonNull
    private ValueEventListener createValueEventListener(final ObservableEmitter<List<Alarm>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readAlarms(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readAlarms(final DataSnapshot dataSnapshot, final ObservableEmitter<List<Alarm>> source) {
        List<Alarm> alarms = getAlarmsFromDataSnapshot(dataSnapshot);
        source.onNext(alarms);
        source.onComplete();
    }

    @NonNull
    private List<Alarm> getAlarmsFromDataSnapshot(final DataSnapshot dataSnapshot) {
        List<Alarm> alarms = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            Alarm alarm = postSnapshot.getValue(Alarm.class);
            if (alarm != null) {
                alarm.setId(postSnapshot.getKey());
                alarms.add(alarm);
            }
        }
        return alarms;
    }

    private void removeAlarm(final String doctorId, final Alarm alarm,
                             final ObservableEmitter<Alarm> source) {
        DatabaseReference databaseReference = getAlarmsDatabaseReference(doctorId)
                .child(alarm.getId());
        databaseReference.removeValue((databaseError, databaseReference1) -> {
            if (databaseError == null) {
                source.onNext(alarm);
                source.onComplete();
            } else {
                source.onError(databaseError.toException());
            }
        });
    }

    private DatabaseReference getAlarmsDatabaseReference(final String doctorId) {
        return databaseReference
                .child("doctors")
                .child(doctorId)
                .child("alarms");
    }
}

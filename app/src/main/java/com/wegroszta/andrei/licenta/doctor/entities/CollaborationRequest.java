package com.wegroszta.andrei.licenta.doctor.entities;

import java.io.Serializable;

public class CollaborationRequest implements Serializable {
    private final String id;
    private final String requesterFirstName;
    private final String requesterLastName;
    private final String requesterId;
    private final long timestmap;

    public CollaborationRequest() {
        this(null, null, null, null, 0L);
    }

    public CollaborationRequest(final String id, final String requesterFirstName, final String requesterLastName,
                                final String requesterId, final long timestmap) {
        this.id = id;
        this.requesterFirstName = requesterFirstName;
        this.requesterLastName = requesterLastName;
        this.requesterId = requesterId;
        this.timestmap = timestmap;
    }

    public String getId() {
        return id;
    }

    public String getRequesterFirstName() {
        return requesterFirstName;
    }

    public String getRequesterLastName() {
        return requesterLastName;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public long getTimestmap() {
        return timestmap;
    }
}

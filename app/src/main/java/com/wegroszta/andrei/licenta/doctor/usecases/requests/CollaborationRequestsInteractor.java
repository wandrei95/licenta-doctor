package com.wegroszta.andrei.licenta.doctor.usecases.requests;

import com.wegroszta.andrei.licenta.doctor.entities.AcceptedPatient;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;

import java.util.List;

import io.reactivex.Observable;

public class CollaborationRequestsInteractor {
    private final CollaborationRequestsStorage collaborationRequestsStorage;
    private final AcceptedPatientsStorage acceptedPatientsStorage;

    public CollaborationRequestsInteractor(final CollaborationRequestsStorage collaborationRequestsStorage,
                                           final AcceptedPatientsStorage acceptedPatientsStorage) {
        this.collaborationRequestsStorage = collaborationRequestsStorage;
        this.acceptedPatientsStorage = acceptedPatientsStorage;
    }

    public Observable<List<CollaborationRequest>> getRequests(final String doctorId) {
        return collaborationRequestsStorage.getRequestsForDoctor(doctorId);
    }

    public Observable<CollaborationRequest> acceptRequest(final String doctorId,
                                                          final CollaborationRequest collaborationRequest) {
        collaborationRequestsStorage.remove(doctorId, collaborationRequest).subscribe();
        return acceptedPatientsStorage.saveAcceptedPatient(doctorId, collaborationRequest);
    }

    public Observable<List<AcceptedPatient>> getPatientsForDoctor(final String doctorId) {
        return acceptedPatientsStorage.getPatientsForDoctor(doctorId);
    }

    public Observable<CollaborationRequest> declineRequest(final String doctorId,
                                                           final CollaborationRequest collaborationRequest) {
        return collaborationRequestsStorage.remove(doctorId, collaborationRequest);
    }

    public Observable<Void> removePatient(final String doctorId, final String patientId) {
        return acceptedPatientsStorage.removeAcceptedPatient(doctorId, patientId);
    }
}

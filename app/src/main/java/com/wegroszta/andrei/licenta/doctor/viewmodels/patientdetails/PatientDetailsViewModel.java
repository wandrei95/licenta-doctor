package com.wegroszta.andrei.licenta.doctor.viewmodels.patientdetails;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.doctor.entities.Patient;
import com.wegroszta.andrei.licenta.doctor.usecases.patientdetails.PatientInteractor;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PatientDetailsViewModel extends ViewModel {
    private final PatientInteractor patientInteractor;
    private final MutableLiveData<Response<Patient>> getPatientResponse;
    private final CompositeDisposable disposables;

    public PatientDetailsViewModel(final PatientInteractor patientInteractor) {
        this.patientInteractor = patientInteractor;
        getPatientResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<Patient>> getGetPatientResponse() {
        return getPatientResponse;
    }

    public void getPatient(final String patientId) {
        disposables.add(patientInteractor.getPatient(patientId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getPatientResponse.setValue(Response.started()))
                .subscribe(
                        patient -> getPatientResponse.setValue(Response.success(patient)),
                        throwable -> getPatientResponse.setValue(Response.failure(throwable)),
                        () -> getPatientResponse.setValue(Response.completed(null))
                )
        );
    }
}

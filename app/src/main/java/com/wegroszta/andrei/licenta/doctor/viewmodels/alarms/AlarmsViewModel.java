package com.wegroszta.andrei.licenta.doctor.viewmodels.alarms;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.doctor.entities.Alarm;
import com.wegroszta.andrei.licenta.doctor.usecases.alarms.AlarmsInteractor;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class AlarmsViewModel extends ViewModel {
    private final AlarmsInteractor alarmsInteractor;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<List<Alarm>>> getAlarmsResponse;
    private final MutableLiveData<Response<Alarm>> removeAlarmResponse;
    private final CompositeDisposable disposables;

    public AlarmsViewModel(final AlarmsInteractor alarmsInteractor, final UserDataFetcher userDataFetcher) {
        this.alarmsInteractor = alarmsInteractor;
        this.userDataFetcher = userDataFetcher;
        getAlarmsResponse = new MutableLiveData<>();
        removeAlarmResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<List<Alarm>>> getGetAlarmsResponse() {
        return getAlarmsResponse;
    }

    public MutableLiveData<Response<Alarm>> getRemoveAlarmResponse() {
        return removeAlarmResponse;
    }

    public void getAlarms() {
        String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(alarmsInteractor.getAlarms(doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getAlarmsResponse.setValue(Response.started()))
                .subscribe(
                        alarms -> getAlarmsResponse.setValue(Response.success(alarms)),
                        throwable -> getAlarmsResponse.setValue(Response.failure(throwable)),
                        () -> getAlarmsResponse.setValue(Response.completed(null))
                )
        );
    }

    public void removeAlarm(final Alarm alarm) {
        String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(alarmsInteractor.removeAlarm(doctorId, alarm)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> removeAlarmResponse.setValue(Response.started()))
                .subscribe(
                        a -> removeAlarmResponse.setValue(Response.success(a)),
                        throwable -> removeAlarmResponse.setValue(Response.failure(throwable)),
                        () -> removeAlarmResponse.setValue(Response.completed(null))
                )
        );
    }
}

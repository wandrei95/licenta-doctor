package com.wegroszta.andrei.licenta.doctor.io.requests;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.doctor.entities.AcceptedPatient;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;
import com.wegroszta.andrei.licenta.doctor.usecases.requests.AcceptedPatientsStorage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseAcceptedPatientsStorage implements AcceptedPatientsStorage {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseAcceptedPatientsStorage(DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<CollaborationRequest> saveAcceptedPatient(
            final String doctorId, final CollaborationRequest collaborationRequest) {
        return Observable.create(source -> acceptCollabReq(doctorId, collaborationRequest, source));
    }

    private void acceptCollabReq(final String doctorId, final CollaborationRequest collaborationRequest,
                                 final ObservableEmitter<CollaborationRequest> source) {
        DatabaseReference databaseReference = getAcceptedPatientsDatabaseReference(doctorId)
                .child(collaborationRequest.getRequesterId());
        AcceptedPatient acceptedPatient = createAcceptedPatientFromRequest(collaborationRequest);
        databaseReference.setValue(acceptedPatient)
                .addOnCompleteListener(getAcceptPatientCompletionListener(collaborationRequest, source))
                .addOnFailureListener(getAcceptPatientErrorListener(source));
    }

    @NonNull
    private AcceptedPatient createAcceptedPatientFromRequest(
            final CollaborationRequest collaborationRequest) {
        return new AcceptedPatient(collaborationRequest.getRequesterId(),
                collaborationRequest.getRequesterFirstName(), collaborationRequest.getRequesterLastName(),
                collaborationRequest.getRequesterId());
    }

    @NonNull
    private OnCompleteListener<Void> getAcceptPatientCompletionListener(
            final CollaborationRequest collaborationRequest,
            final ObservableEmitter<CollaborationRequest> source) {
        return task -> {
            source.onNext(collaborationRequest);
            source.onComplete();
        };
    }

    @NonNull
    private OnFailureListener getAcceptPatientErrorListener(
            final ObservableEmitter<CollaborationRequest> source) {
        return source::onError;
    }

    @Override
    public Observable<List<AcceptedPatient>> getPatientsForDoctor(final String doctorId) {
        return Observable.create(source -> getPatients(doctorId, source));
    }

    private void getPatients(final String doctorId,
                             final ObservableEmitter<List<AcceptedPatient>> source) {
        DatabaseReference databaseReference = getAcceptedPatientsDatabaseReference(doctorId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    @NonNull
    private ValueEventListener createValueEventListener(
            final ObservableEmitter<List<AcceptedPatient>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readPatients(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readPatients(final DataSnapshot dataSnapshot,
                              final ObservableEmitter<List<AcceptedPatient>> source) {
        List<AcceptedPatient> patients = getAcceptedPatientsFromDataSnapshot(dataSnapshot);
        source.onNext(patients);
        source.onComplete();
    }

    @NonNull
    private List<AcceptedPatient> getAcceptedPatientsFromDataSnapshot(
            final DataSnapshot dataSnapshot) {
        List<AcceptedPatient> patients = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            AcceptedPatient patient = postSnapshot.getValue(AcceptedPatient.class);
            patients.add(patient);
        }
        return patients;
    }

    private DatabaseReference getAcceptedPatientsDatabaseReference(final String doctorId) {
        return baseDatabaseReference
                .child("doctors")
                .child(doctorId)
                .child("acceptedPatients");
    }

    @Override
    public Observable<Void> removeAcceptedPatient(String doctorId, String patientId) {
        return Observable.create(source -> removeAcceptedPatient(doctorId, patientId, source));
    }

    private void removeAcceptedPatient(final String doctorId, final String patientId,
                                       final ObservableEmitter<Void> source) {
        DatabaseReference databaseReference = getAcceptedPatientsDatabaseReference(doctorId)
                .child(patientId);
        databaseReference.removeValue((databaseError, databaseReference1) -> {
            if (databaseError == null) {
                source.onComplete();
            } else {
                source.onError(databaseError.toException());
            }
        });
    }
}

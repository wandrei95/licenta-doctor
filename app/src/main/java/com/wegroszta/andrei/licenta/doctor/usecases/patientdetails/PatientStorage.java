package com.wegroszta.andrei.licenta.doctor.usecases.patientdetails;

import com.wegroszta.andrei.licenta.doctor.entities.Patient;

import io.reactivex.Observable;

public interface PatientStorage {
    Observable<Patient> getPatientForId(String patientId);
}

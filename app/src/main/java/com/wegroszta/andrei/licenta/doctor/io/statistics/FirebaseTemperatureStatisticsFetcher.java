package com.wegroszta.andrei.licenta.doctor.io.statistics;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.doctor.entities.Temperature;
import com.wegroszta.andrei.licenta.doctor.usecases.statistics.temperature.TemperatureStatisticsFetcher;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseTemperatureStatisticsFetcher implements TemperatureStatisticsFetcher {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseTemperatureStatisticsFetcher(DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<List<Temperature>> getTemperatures(final String patientId) {
        return Observable.create(source -> fetchTemperatures(patientId, source));
    }

    private void fetchTemperatures(final String patientId,
                                   final ObservableEmitter<List<Temperature>> source) {
        DatabaseReference databaseReference = getDatabaseReference(patientId);
        databaseReference.addValueEventListener(createValueEventListener(source));
    }

    @NonNull
    private ValueEventListener createValueEventListener(
            final ObservableEmitter<List<Temperature>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readTemperatures(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readTemperatures(final DataSnapshot dataSnapshot,
                                  final ObservableEmitter<List<Temperature>> source) {
        List<Temperature> temperatures = getTemperaturesFromDataSnapshot(dataSnapshot);
        source.onNext(temperatures);
        source.onComplete();
    }

    @NonNull
    private List<Temperature> getTemperaturesFromDataSnapshot(final DataSnapshot dataSnapshot) {
        List<Temperature> temperatures = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            Temperature temperature = postSnapshot.getValue(Temperature.class);
            temperatures.add(temperature);
        }
        return temperatures;
    }

    private DatabaseReference getDatabaseReference(final String patientId) {
        return baseDatabaseReference
                .child("measurement")
                .child("temperature")
                .child(patientId)
                .child("values");
    }
}
package com.wegroszta.andrei.licenta.doctor.entities;

public class StateData extends Data {
    private final State state;

    public StateData() {
        this(0L, State.AVG);
    }

    public StateData(final long timestamp, final State state) {
        super(timestamp);
        this.state = state;
    }

    public State getState() {
        return state;
    }
}

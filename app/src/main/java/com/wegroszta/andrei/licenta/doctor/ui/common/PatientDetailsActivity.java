package com.wegroszta.andrei.licenta.doctor.ui.common;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;
import com.wegroszta.andrei.licenta.doctor.entities.Patient;
import com.wegroszta.andrei.licenta.doctor.ui.util.ValidationUtil;
import com.wegroszta.andrei.licenta.doctor.ui.util.ViewModelFactory;
import com.wegroszta.andrei.licenta.doctor.viewmodels.patientdetails.PatientDetailsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.requests.CollaborationRequestsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String COLLABORATION_REQUEST_KEY = "collab_req_key";
    public static final String PATIENT_ID_KEY = "patient_id_key";
    public static final String PATIENT_ID = "patient_id";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_first_name)
    TextView tvFirstName;
    @BindView(R.id.tv_last_name)
    TextView tvLastName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_phone_nr)
    TextView tvPhoneNr;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.btn_view_patient_measurements)
    Button btnViewPatientMeasurements;
    @BindView(R.id.loading)
    ProgressBar loading;

    private CollaborationRequest collaborationRequest;
    private String patientId;
    private PatientDetailsViewModel patientDetailsViewModel;
    private CollaborationRequestsViewModel collaborationRequestsViewModel;
    private Menu menu;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);

        ButterKnife.bind(this);

        setActionBar();

        setupViewModels();

        setClickListeners();
    }

    private void setActionBar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    private void setClickListeners() {
        btnViewPatientMeasurements.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_patient_details, menu);
        extractDataFromIntent();
        getPatientDetails();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_accept:
                acceptCollaborationRequest();
                return true;
            case R.id.action_decline:
                declineCollaborationRequest();
                return true;
            case R.id.action_remove_patient:
                removePatient();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewModels() {
        setupPatientDetailsViewModel();
        setupCollaborationRequestsViewModel();
    }

    private void setupPatientDetailsViewModel() {
        patientDetailsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(PatientDetailsViewModel.class);
        patientDetailsViewModel.getGetPatientResponse().observe(this, this::processGetPatientResponse);
    }

    private void setupCollaborationRequestsViewModel() {
        collaborationRequestsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(CollaborationRequestsViewModel.class);
        collaborationRequestsViewModel.getAcceptCollaborationRequestResponse().observe(this,
                this::processAcceptCollaborationRequestResponse);
        collaborationRequestsViewModel.getDeclineCollaborationRequestResponse().observe(this,
                this::processDeclineCollaborationRequestResponse);
        collaborationRequestsViewModel.getRemovePatientResponse().observe(this,
                this::processRemovePatientResponse);
    }

    private void processGetPatientResponse(final Response<Patient> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onPatientLoaded(response.data);
                break;
            case COMPLETED:
                onLoadPatientEnd();
                break;
            case FAILURE:
                onLoadPatientFail(response.error);
                break;
        }
    }

    private void processAcceptCollaborationRequestResponse(final Response<CollaborationRequest> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onCollaborationRequestAccepted(response.data);
                break;
            case COMPLETED:
                onCollaborationRequestAcceptedFinished();
                break;
            case FAILURE:
                onCollaborationRequestAcceptFail(response.error);
                break;
        }
    }

    private void processDeclineCollaborationRequestResponse(final Response<CollaborationRequest> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onCollaborationRequestDeclined(response.data);
                break;
            case COMPLETED:
                onCollaborationRequestDeclinedFinished();
                break;
            case FAILURE:
                onCollaborationRequestDeclineFail(response.error);
                break;
        }
    }

    private void processRemovePatientResponse(final Response<Void> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case COMPLETED:
                finish();
                break;
            case FAILURE:
                onCollaborationRequestDeclineFail(response.error);
                break;
        }
    }

    private void extractDataFromIntent() {
        Intent intent = getIntent();

        if (intent != null) {
            Bundle extras = intent.getExtras();
            extractCollaborationRequestData(extras);
        }
    }

    private void extractCollaborationRequestData(final Bundle extras) {
        if (extras != null) {
            if (extras.containsKey(COLLABORATION_REQUEST_KEY)) {
                collaborationRequest = (CollaborationRequest) extras.getSerializable(COLLABORATION_REQUEST_KEY);
                if (collaborationRequest != null) {
                    patientId = collaborationRequest.getRequesterId();
                }

                showAcceptDeclineButtons();
                menu.findItem(R.id.action_remove_patient).setVisible(false);
            } else if (extras.containsKey(PATIENT_ID_KEY)) {
                patientId = extras.getString(PATIENT_ID_KEY);
                hideAcceptDeclineButtons();
                menu.findItem(R.id.action_remove_patient).setVisible(true);
            }
        }
    }

    private void showAcceptDeclineButtons() {
        menu.findItem(R.id.action_accept).setVisible(true);
        menu.findItem(R.id.action_decline).setVisible(true);
        menu.findItem(R.id.action_remove_patient).setVisible(false);
    }

    private void hideAcceptDeclineButtons() {
        menu.findItem(R.id.action_accept).setVisible(false);
        menu.findItem(R.id.action_decline).setVisible(false);
        menu.findItem(R.id.action_remove_patient).setVisible(true);
    }

    private void getPatientDetails() {
        patientDetailsViewModel.getPatient(patientId);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void onPatientLoaded(final Patient patient) {
        tvFirstName.setText(ValidationUtil.getFieldValueToShow(patient.getFirstName()));
        tvLastName.setText(ValidationUtil.getFieldValueToShow(patient.getLastName()));
        tvEmail.setText(ValidationUtil.getFieldValueToShow(patient.getEmail()));
        tvPhoneNr.setText(ValidationUtil.getFieldValueToShow(patient.getPhone()));
        tvAddress.setText(ValidationUtil.getFieldValueToShow(patient.getAddress()));
    }

    private void onLoadPatientFail(final Throwable cause) {
        hideLoadingState();
        cause.printStackTrace();
        Toast.makeText(this, R.string.load_patient_error_msg, Toast.LENGTH_LONG).show();
    }

    private void onLoadPatientEnd() {
        hideLoadingState();
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btn_view_patient_measurements:
                goToPatientMeasurements();
                break;
        }
    }

    private void goToPatientMeasurements() {
        Intent intent = new Intent(this, PatientStatisticsActivity.class);
        intent.putExtra(PatientStatisticsActivity.PATIENT_ID_KEY, patientId);
        startActivity(intent);
    }

    private void acceptCollaborationRequest() {
        collaborationRequestsViewModel.acceptCollaborationRequest(collaborationRequest);
    }

    private void onCollaborationRequestAccepted(CollaborationRequest collaborationRequest) {
        Toast.makeText(this, R.string.collab_req_accepted_msg, Toast.LENGTH_LONG).show();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(PATIENT_ID, collaborationRequest.getId());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void onCollaborationRequestAcceptFail(final Throwable cause) {
        cause.printStackTrace();
        Toast.makeText(this, R.string.unkown_error, Toast.LENGTH_LONG).show();
    }

    private void onCollaborationRequestAcceptedFinished() {
        hideLoadingState();
    }

    private void declineCollaborationRequest() {
        collaborationRequestsViewModel.declineCollaborationRequest(collaborationRequest);
    }

    private void removePatient() {
        collaborationRequestsViewModel.removePatient(patientId);
    }

    private void onCollaborationRequestDeclined(final CollaborationRequest collaborationRequest) {
        Toast.makeText(this, R.string.collab_req_declined_msg, Toast.LENGTH_LONG).show();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(PATIENT_ID, collaborationRequest.getId());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void onCollaborationRequestDeclineFail(final Throwable cause) {
        cause.printStackTrace();
        hideLoadingState();
        Toast.makeText(this, R.string.unkown_error, Toast.LENGTH_LONG).show();
    }

    private void onCollaborationRequestDeclinedFinished() {
        hideLoadingState();
    }
}

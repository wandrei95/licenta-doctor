package com.wegroszta.andrei.licenta.doctor.usecases.auth;

import com.wegroszta.andrei.licenta.doctor.entities.Credentials;
import com.wegroszta.andrei.licenta.doctor.entities.User;

import io.reactivex.Observable;

public interface UserCreator {
    Observable<Void> signUp(Credentials credentials, User user);
}

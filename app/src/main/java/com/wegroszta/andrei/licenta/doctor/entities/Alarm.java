package com.wegroszta.andrei.licenta.doctor.entities;

public class Alarm {
    private String id;
    private final String patientId;
    private final String patientFirstName;
    private final String patientLastName;
    private final String alarmType;
    private final long timestamp;
    private final double value;

    public Alarm() {
        this(null, null, null, null, null, 0L, 0d);
    }

    public Alarm(final String id, final String patientId, final String patientFirstName,
                 final String patientLastName, final String alarmType,
                 final long timestamp, final double value) {
        this.id = id;
        this.patientId = patientId;
        this.patientFirstName = patientFirstName;
        this.patientLastName = patientLastName;
        this.alarmType = alarmType;
        this.timestamp = timestamp;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getPatientId() {
        return patientId;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public String getAlarmType() {
        return alarmType;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getValue() {
        return value;
    }

    public void setId(String id) {
        this.id = id;
    }
}

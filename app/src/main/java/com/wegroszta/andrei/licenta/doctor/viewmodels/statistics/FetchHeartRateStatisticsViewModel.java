package com.wegroszta.andrei.licenta.doctor.viewmodels.statistics;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.doctor.entities.HeartRate;
import com.wegroszta.andrei.licenta.doctor.usecases.statistics.heartrate.FetchHeartRateStatistics;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FetchHeartRateStatisticsViewModel extends ViewModel {
    private final FetchHeartRateStatistics fetchHeartRateStatistics;
    private final MutableLiveData<Response<List<HeartRate>>> fetchHeartRateResponse;
    private final CompositeDisposable disposables;

    public FetchHeartRateStatisticsViewModel(final FetchHeartRateStatistics fetchHeartRateStatistics) {
        this.fetchHeartRateStatistics = fetchHeartRateStatistics;
        fetchHeartRateResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<List<HeartRate>>> getFetchHeartRateResponse() {
        return fetchHeartRateResponse;
    }

    public void getHeartRates(final String patientId) {
        disposables.add(fetchHeartRateStatistics.observeHeartRateStatistics(patientId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> fetchHeartRateResponse.setValue(Response.started()))
                .subscribe(
                        heartRates -> fetchHeartRateResponse.setValue(Response.success(heartRates)),
                        throwable -> fetchHeartRateResponse.setValue(Response.failure(throwable)),
                        () -> fetchHeartRateResponse.setValue(Response.completed(null))
                )
        );
    }
}

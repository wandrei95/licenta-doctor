package com.wegroszta.andrei.licenta.doctor.entities;

public enum State {
    BELOW_AVG,
    AVG,
    ABOVE_AVG,
    ABNORMALITY
}

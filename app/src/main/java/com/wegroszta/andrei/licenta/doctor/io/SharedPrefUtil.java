package com.wegroszta.andrei.licenta.doctor.io;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtil {
    private static final String NAME = "licenta-doc";
    private static final String FCM_TOKEN = "FCM_TOKEN";

    private static final Object lock = new Object();
    @SuppressLint("StaticFieldLeak")
    private static SharedPrefUtil instance;

    private Context context;

    private SharedPrefUtil() {

    }

    public static SharedPrefUtil getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new SharedPrefUtil();
                }
            }
        }
        return instance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void saveFcmToken(String fcmToken) {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(FCM_TOKEN, fcmToken);
        editor.apply();
    }

    public String getFcmToken() {
        SharedPreferences sharedPref = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sharedPref.getString(FCM_TOKEN, "");
    }
}

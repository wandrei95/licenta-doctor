package com.wegroszta.andrei.licenta.doctor.entities;

public class Data {
    private final long timestamp;

    public Data() {
        this(0L);
    }

    public Data(final long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }
}

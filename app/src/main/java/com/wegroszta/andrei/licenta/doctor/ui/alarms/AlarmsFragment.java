package com.wegroszta.andrei.licenta.doctor.ui.alarms;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.Alarm;
import com.wegroszta.andrei.licenta.doctor.ui.common.PatientDetailsActivity;
import com.wegroszta.andrei.licenta.doctor.ui.util.ViewModelFactory;
import com.wegroszta.andrei.licenta.doctor.viewmodels.alarms.AlarmsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AlarmsFragment extends Fragment implements SearchView.OnQueryTextListener, AlarmsRecyclerViewAdapter.OnAlarmClickListener {
    @BindView(R.id.rv_alarms)
    RecyclerView rvAlarms;
    @BindView(R.id.tv_no_alarms)
    TextView tvNoAlarms;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.loading)
    ProgressBar loading;

    private AlarmsRecyclerViewAdapter adapter;
    private AlarmsViewModel alarmsViewModel;
    private Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alarms, container, false);

        unbinder = ButterKnife.bind(this, view);

        setupAlarmsViewModel();

        setupRequestsRecyclerView();
        setupSearchView();
        loadRequests();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setupAlarmsViewModel() {
        alarmsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(AlarmsViewModel.class);
        alarmsViewModel.getGetAlarmsResponse().observe(this, this::processGetAlarmsResponse);
        alarmsViewModel.getRemoveAlarmResponse().observe(this, this::processRemoveAlarmResponse);
    }

    private void processGetAlarmsResponse(final Response<List<Alarm>> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onAlarmsLoaded(response.data);
                break;
            case COMPLETED:
                onLoadRequestsEnd();
                break;
            case FAILURE:
                onLoadAlarmsFail(response.error);
                break;
        }
    }

    private void processRemoveAlarmResponse(final Response<Alarm> response) {
        switch (response.state) {
            case COMPLETED:
                adapter.removeAlarm(response.data.getId());
                break;
        }
    }

    private void setupRequestsRecyclerView() {
        adapter = new AlarmsRecyclerViewAdapter();
        adapter.setOnAlarmClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvAlarms.setLayoutManager(layoutManager);
        rvAlarms.setAdapter(adapter);
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(this);
    }

    private void loadRequests() {
        tvNoAlarms.setVisibility(View.GONE);
        rvAlarms.setVisibility(View.GONE);

        alarmsViewModel.getAlarms();
    }

    private void onAlarmsLoaded(final List<Alarm> alarms) {
        if (alarms.size() > 0) {
            tvNoAlarms.setVisibility(View.GONE);
            rvAlarms.setVisibility(View.VISIBLE);
            adapter.setAlarms(alarms);
        } else {
            tvNoAlarms.setVisibility(View.VISIBLE);
        }
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void onLoadAlarmsFail(final Throwable cause) {
        cause.printStackTrace();
        hideLoadingState();
        tvNoAlarms.setVisibility(View.VISIBLE);
        Toast.makeText(getActivity(), R.string.load_requests_error_msg, Toast.LENGTH_LONG).show();
    }

    private void onLoadRequestsEnd() {
        hideLoadingState();
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        adapter.filter(newText);
        return false;
    }

    @Override
    public void onAlarmClicked(final Alarm alarm) {
        Intent intent = new Intent(getActivity(), PatientDetailsActivity.class);
        intent.putExtra(PatientDetailsActivity.PATIENT_ID_KEY, alarm.getPatientId());
        startActivity(intent);
        alarmsViewModel.removeAlarm(alarm);
    }
}

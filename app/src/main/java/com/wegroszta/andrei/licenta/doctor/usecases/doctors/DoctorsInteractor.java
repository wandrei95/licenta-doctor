package com.wegroszta.andrei.licenta.doctor.usecases.doctors;

import com.wegroszta.andrei.licenta.doctor.entities.Doctor;

import io.reactivex.Observable;

public class DoctorsInteractor {
    private final DoctorsStorage doctorsStorage;

    public DoctorsInteractor(final DoctorsStorage doctorsStorage) {
        this.doctorsStorage = doctorsStorage;
    }

    public Observable<Doctor> getDoctor(final String id) {
        return doctorsStorage.getDoctor(id);
    }

    public Observable<Void> saveDoctor(final Doctor doctor) {
        return doctorsStorage.saveDoctor(doctor);
    }
}

package com.wegroszta.andrei.licenta.doctor.ui.requests;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RequestsRecycleViewAdapter extends RecyclerView.Adapter<RequestsRecycleViewAdapter.RequestsViewHolder> {
    private final List<CollaborationRequest> requests;
    private final List<CollaborationRequest> filteredRequests;
    private OnRequestClickListener onRequestClickListener;

    RequestsRecycleViewAdapter() {
        this.requests = new ArrayList<>();
        this.filteredRequests = new ArrayList<>();
    }

    public void setRequests(final List<CollaborationRequest> requests) {
        this.requests.clear();
        this.requests.addAll(requests);
        this.filteredRequests.clear();
        this.filteredRequests.addAll(requests);
        notifyDataSetChanged();
    }

    public void setOnRequestClickListener(OnRequestClickListener onRequestClickListener) {
        this.onRequestClickListener = onRequestClickListener;
    }

    @NonNull
    @Override
    public RequestsViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.request_layout, parent, false);
        return new RequestsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RequestsViewHolder holder, final int position) {
        CollaborationRequest request = filteredRequests.get(position);

        holder.tvFirstName.setText(request.getRequesterFirstName());
        holder.tvLastName.setText(request.getRequesterLastName());

        holder.container.setOnClickListener(v -> {
            if (onRequestClickListener != null) {
                onRequestClickListener.onRequestClicked(request);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredRequests.size();
    }

    public void filter(final String filterText) {
        filteredRequests.clear();

        if (filterText == null || filterText.trim().isEmpty()) {
            filteredRequests.addAll(requests);
            notifyDataSetChanged();
        } else {
            for (CollaborationRequest request : requests) {
                if (request.getRequesterFirstName().toUpperCase().contains(filterText.toUpperCase())
                        || request.getRequesterLastName().toUpperCase().contains(filterText.toUpperCase())) {
                    filteredRequests.add(request);
                }
            }
            notifyDataSetChanged();
        }
    }

    public void removeCollaborationRequest(final String collaborationRequestId) {
        Iterator<CollaborationRequest> iterator = requests.iterator();
        while (iterator.hasNext()) {
            CollaborationRequest collaborationRequest = iterator.next();
            if (collaborationRequest.getId().equals(collaborationRequestId)) {
                iterator.remove();
                filteredRequests.remove(collaborationRequest);
                notifyDataSetChanged();
                break;
            }
        }
    }

    class RequestsViewHolder extends RecyclerView.ViewHolder {
        final TextView tvFirstName;
        final TextView tvLastName;
        final View container;

        RequestsViewHolder(final View itemView) {
            super(itemView);
            tvFirstName = itemView.findViewById(R.id.tv_first_name);
            tvLastName = itemView.findViewById(R.id.tv_last_name);
            container = itemView;
        }
    }

    public interface OnRequestClickListener {
        void onRequestClicked(CollaborationRequest request);
    }
}

package com.wegroszta.andrei.licenta.doctor.ui.util;

import android.util.Patterns;

public class ValidationUtil {
    private ValidationUtil() {

    }

    public static boolean isStringNullOrEmpty(final String s) {
        return s == null || s.trim().isEmpty();
    }

    public static boolean isEmailFormatInvalid(final String email) {
        return email == null || !Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordLengthInvalid(final String password) {
        return password == null || password.length() < 6;
    }

    public static String getFieldValueToShow(final String fieldValue) {
        if (fieldValue == null || fieldValue.trim().isEmpty()) {
            return "";
        } else {
            return fieldValue;
        }
    }
}

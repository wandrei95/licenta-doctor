package com.wegroszta.andrei.licenta.doctor.usecases.auth;

public interface UserStorage {
    String getLoggedUserId();
}

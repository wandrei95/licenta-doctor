/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wegroszta.andrei.licenta.doctor.fcm;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.wegroszta.andrei.licenta.doctor.io.SharedPrefUtil;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "BBBBBBBBBBBBBBBB";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            String userId = firebaseUser.getUid();
            DatabaseReference baseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            baseDatabaseReference.child("doctors").child(userId).child("fcmToken")
                    .setValue(refreshedToken)
                    .addOnCompleteListener(t -> {
                    })
                    .addOnFailureListener(e -> {
                    });
        }
        SharedPrefUtil.getInstance().saveFcmToken(refreshedToken);
    }
}

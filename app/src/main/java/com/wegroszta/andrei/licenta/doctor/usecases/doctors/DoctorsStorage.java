package com.wegroszta.andrei.licenta.doctor.usecases.doctors;

import com.wegroszta.andrei.licenta.doctor.entities.Doctor;

import io.reactivex.Observable;

public interface DoctorsStorage {
    Observable<Doctor> getDoctor(String id);

    Observable<Void> saveDoctor(Doctor doctor);
}

package com.wegroszta.andrei.licenta.doctor.ui.alarms;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.Alarm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AlarmsRecyclerViewAdapter extends RecyclerView.Adapter<AlarmsRecyclerViewAdapter.AlarmsViewHolder> {
    private final List<Alarm> alarms;
    private final List<Alarm> filteredAlarms;
    private OnAlarmClickListener onAlarmClickListener;

    AlarmsRecyclerViewAdapter() {
        this.alarms = new ArrayList<>();
        this.filteredAlarms = new ArrayList<>();
    }

    public void setAlarms(final List<Alarm> alarms) {
        this.alarms.clear();
        this.alarms.addAll(alarms);
        this.filteredAlarms.clear();
        this.filteredAlarms.addAll(alarms);
        notifyDataSetChanged();
    }

    public void setOnAlarmClickListener(final OnAlarmClickListener onAlarmClickListener) {
        this.onAlarmClickListener = onAlarmClickListener;
    }

    @NonNull
    @Override
    public AlarmsRecyclerViewAdapter.AlarmsViewHolder onCreateViewHolder(
            @NonNull final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.alarm_layout, parent, false);
        return new AlarmsRecyclerViewAdapter.AlarmsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final AlarmsRecyclerViewAdapter.AlarmsViewHolder holder,
                                 final int position) {
        Alarm alarm = filteredAlarms.get(position);

        holder.tvPatientFirstName.setText(alarm.getPatientFirstName());
        holder.tvPatientLastName.setText(alarm.getPatientLastName());
        holder.tvAlarmType.setText(alarm.getAlarmType());

        holder.container.setOnClickListener(v -> {
            if (onAlarmClickListener != null) {
                onAlarmClickListener.onAlarmClicked(alarm);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredAlarms.size();
    }

    public void filter(final String filterText) {
        filteredAlarms.clear();

        if (filterText == null || filterText.trim().isEmpty()) {
            filteredAlarms.addAll(alarms);
            notifyDataSetChanged();
        } else {
            for (Alarm alarm : alarms) {
                if (alarm.getPatientFirstName().toUpperCase().contains(filterText.toUpperCase())
                        || alarm.getPatientLastName().toUpperCase().contains(filterText.toUpperCase())
                        || alarm.getAlarmType().toUpperCase().contains(filterText.toUpperCase())) {
                    filteredAlarms.add(alarm);
                }
            }
            notifyDataSetChanged();
        }
    }

    public void removeAlarm(final String alarmId) {
        Iterator<Alarm> iterator = alarms.iterator();
        while (iterator.hasNext()) {
            Alarm alarm = iterator.next();
            if (alarm.getId().equals(alarmId)) {
                iterator.remove();
                filteredAlarms.remove(alarm);
                notifyDataSetChanged();
                break;
            }
        }
    }

    class AlarmsViewHolder extends RecyclerView.ViewHolder {
        final TextView tvPatientFirstName;
        final TextView tvPatientLastName;
        final TextView tvAlarmType;
        final View container;

        AlarmsViewHolder(final View itemView) {
            super(itemView);
            tvPatientFirstName = itemView.findViewById(R.id.tv_first_name);
            tvPatientLastName = itemView.findViewById(R.id.tv_last_name);
            tvAlarmType = itemView.findViewById(R.id.tv_type);
            container = itemView;
        }
    }

    public interface OnAlarmClickListener {
        void onAlarmClicked(Alarm alarm);
    }
}
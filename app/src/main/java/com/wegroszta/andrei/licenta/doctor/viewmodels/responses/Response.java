package com.wegroszta.andrei.licenta.doctor.viewmodels.responses;

import android.support.annotation.NonNull;

public class Response<T> {
    public final T data;
    public final State state;
    public final Throwable error;

    private Response(final T data, final State state, final Throwable error) {
        this.data = data;
        this.state = state;
        this.error = error;
    }

    @NonNull
    public static <T> Response<T> started() {
        return new Response<>(null, State.STARTED, null);
    }

    @NonNull
    public static <T> Response<T> success(final T data) {
        return new Response<>(data, State.SUCCESS, null);
    }

    @NonNull
    public static <T> Response<T> completed(final T data) {
        return new Response<>(data, State.COMPLETED, null);
    }

    @NonNull
    public static <T> Response<T> failure(final Throwable e) {
        return new Response<>(null, State.FAILURE, e);
    }
}

package com.wegroszta.andrei.licenta.doctor.ui.common;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wegroszta.andrei.licenta.doctor.ui.alarms.AlarmsFragment;
import com.wegroszta.andrei.licenta.doctor.ui.mypatients.MyPatientsFragment;
import com.wegroszta.andrei.licenta.doctor.ui.myprofile.MyProfileFragment;
import com.wegroszta.andrei.licenta.doctor.ui.requests.RequestsFragment;
import com.wegroszta.andrei.licenta.doctor.ui.settings.SettingsFragment;

import java.util.ArrayList;
import java.util.List;

public class MainScreensFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final List<Fragment> fragments = new ArrayList<>();

    static {
        fragments.add(new RequestsFragment());
        fragments.add(new AlarmsFragment());
        fragments.add(new MyPatientsFragment());
        fragments.add(new MyProfileFragment());
        fragments.add(new SettingsFragment());
    }

    MainScreensFragmentPagerAdapter(@NonNull final FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(final int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}

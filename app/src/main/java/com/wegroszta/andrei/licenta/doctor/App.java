package com.wegroszta.andrei.licenta.doctor;

import android.app.Application;

import com.wegroszta.andrei.licenta.doctor.io.SharedPrefUtil;
import com.wegroszta.andrei.licenta.doctor.ui.util.SettingsPreferencesUtil;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPrefUtil.getInstance().setContext(this);
        SettingsPreferencesUtil.setContext(this);
    }
}

package com.wegroszta.andrei.licenta.doctor.ui.myprofile;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.Doctor;
import com.wegroszta.andrei.licenta.doctor.ui.util.UITextUtil;
import com.wegroszta.andrei.licenta.doctor.ui.util.ViewModelFactory;
import com.wegroszta.andrei.licenta.doctor.viewmodels.doctors.DoctorsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MyProfileFragment extends Fragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone_nr)
    EditText etPhone;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_specialty)
    EditText etSpecialty;
    @BindView(R.id.loading)
    ProgressBar loading;

    private Unbinder unbinder;
    private DoctorsViewModel doctorsViewModel;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        final View mainView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        unbinder = ButterKnife.bind(this, mainView);

        setActionBar();

        setupDoctorsViewModel();
        getData();

        return mainView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_my_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setActionBar() {
        toolbar.setTitle("");
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_my_profile:
                saveChanges();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void saveChanges() {
        Doctor doctor = getDoctorFromFields();
        doctorsViewModel.saveDoctorDetails(doctor);
    }

    @NonNull
    private Doctor getDoctorFromFields() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();
        String address = etAddress.getText().toString();
        String specialty = etSpecialty.getText().toString();
        return new Doctor(firstName, lastName, email, phone, address, specialty);
    }

    private void setupDoctorsViewModel() {
        doctorsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(DoctorsViewModel.class);
        doctorsViewModel.getGetDoctorResponse()
                .observe(this, this::processDoctorResponse);
        doctorsViewModel.getSaveDoctorResponse()
                .observe(this, this::processSaveDoctorResponse);
    }

    private void processDoctorResponse(final Response<Doctor> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onDoctorLoaded(response.data);
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.error);
                break;
        }
    }

    private void processSaveDoctorResponse(final Response<Void> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onPatientSaved();
                break;
            case COMPLETED:
                hideLoadingState();
                break;
            case FAILURE:
                onError(response.error);
                break;
        }
    }

    private void onPatientSaved() {
        hideLoadingState();
        Toast.makeText(getActivity(), R.string.saving_data_success_msg, Toast.LENGTH_LONG).show();
    }

    private void onDoctorLoaded(final Doctor doctor) {
        UITextUtil.setTextToEditText(etFirstName, doctor.getFirstName());
        UITextUtil.setTextToEditText(etLastName, doctor.getLastName());
        UITextUtil.setTextToEditText(etEmail, doctor.getEmail());
        UITextUtil.setTextToEditText(etPhone, doctor.getPhone());
        UITextUtil.setTextToEditText(etAddress, doctor.getAddress());
        UITextUtil.setTextToEditText(etSpecialty, doctor.getSpecialty());
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void onError(final Throwable throwable) {
        hideLoadingState();
        throwable.printStackTrace();
        Toast.makeText(getActivity(), R.string.something_went_wrong, Toast.LENGTH_LONG).show();
    }

    public void getData() {
        doctorsViewModel.getLoggedDoctor();
    }
}
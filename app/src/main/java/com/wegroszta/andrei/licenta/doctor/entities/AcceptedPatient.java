package com.wegroszta.andrei.licenta.doctor.entities;

public class AcceptedPatient {
    private final String id;
    private final String firstName;
    private final String lastName;
    private final String acceptanceId;

    public AcceptedPatient() {
        this(null, null, null, null);
    }

    public AcceptedPatient(final String id, final String firstName, final String lastName,
                           final String acceptanceId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.acceptanceId = acceptanceId;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAcceptanceId() {
        return acceptanceId;
    }
}

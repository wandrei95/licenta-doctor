package com.wegroszta.andrei.licenta.doctor.viewmodels.responses;

public enum State {
    STARTED,
    SUCCESS,
    COMPLETED,
    FAILURE
}

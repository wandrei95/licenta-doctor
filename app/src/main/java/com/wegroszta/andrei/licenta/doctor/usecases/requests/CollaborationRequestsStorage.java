package com.wegroszta.andrei.licenta.doctor.usecases.requests;

import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;

import java.util.List;

import io.reactivex.Observable;

public interface CollaborationRequestsStorage {
    Observable<List<CollaborationRequest>> getRequestsForDoctor(String doctorId);

    Observable<CollaborationRequest> remove(String doctorId, CollaborationRequest collaborationRequest);
}

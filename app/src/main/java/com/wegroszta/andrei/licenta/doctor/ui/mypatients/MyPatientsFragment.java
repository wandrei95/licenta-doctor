package com.wegroszta.andrei.licenta.doctor.ui.mypatients;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.AcceptedPatient;
import com.wegroszta.andrei.licenta.doctor.ui.common.PatientDetailsActivity;
import com.wegroszta.andrei.licenta.doctor.ui.util.ViewModelFactory;
import com.wegroszta.andrei.licenta.doctor.viewmodels.requests.CollaborationRequestsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyPatientsFragment extends Fragment implements AcceptedPatientRecyclerViewAdapter.OnPatientClickListener, SearchView.OnQueryTextListener {
    private static final int REMOVE_PATIENT_REQUEST_CODE = 234;

    @BindView(R.id.rv_my_patients)
    RecyclerView rvMyPatients;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.loading)
    ProgressBar loading;

    private AcceptedPatientRecyclerViewAdapter adapter;
    private CollaborationRequestsViewModel collaborationRequestsViewModel;
    private Unbinder unbinder;

    public MyPatientsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_patients, container, false);

        unbinder = ButterKnife.bind(this, view);

        setupRecyclerViewAdapter();

        setupCollaborationRequestsViewModel();

        searchView.setOnQueryTextListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadPatients();
    }

    private void setupCollaborationRequestsViewModel() {
        collaborationRequestsViewModel = ViewModelProviders.of(this, ViewModelFactory.getInstance())
                .get(CollaborationRequestsViewModel.class);
        collaborationRequestsViewModel.getGetMyPatientsResponse().observe(this,
                this::processGetMyPatientsResponse);
    }

    private void processGetMyPatientsResponse(final Response<List<AcceptedPatient>> response) {
        switch (response.state) {
            case STARTED:
                showLoadingState();
                break;
            case SUCCESS:
                onPatientsLoaded(response.data);
                break;
            case COMPLETED:
                onPatientsLoadFinish();
                break;
            case FAILURE:
                onPatientsLoadFailure(response.error);
                break;
        }
    }

    private void loadPatients() {
        collaborationRequestsViewModel.getMyPatients();
    }

    private void hideLoadingState() {
        loading.setVisibility(View.GONE);
    }

    private void showLoadingState() {
        loading.setVisibility(View.VISIBLE);
    }

    private void onPatientsLoaded(final List<AcceptedPatient> acceptedPatients) {
        adapter.setPatients(acceptedPatients);
    }

    private void onPatientsLoadFailure(final Throwable throwable) {
        throwable.printStackTrace();
        hideLoadingState();
        Toast.makeText(getActivity(), R.string.unkown_error, Toast.LENGTH_LONG).show();
    }

    private void onPatientsLoadFinish() {
        hideLoadingState();
    }

    private void setupRecyclerViewAdapter() {
        adapter = new AcceptedPatientRecyclerViewAdapter();
        adapter.setOnPatientClickListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMyPatients.setLayoutManager(layoutManager);
        rvMyPatients.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPatientClicked(final AcceptedPatient acceptedPatient) {
        Intent intent = new Intent(getActivity(), PatientDetailsActivity.class);
        intent.putExtra(PatientDetailsActivity.PATIENT_ID_KEY, acceptedPatient.getId());
        startActivityForResult(intent, REMOVE_PATIENT_REQUEST_CODE);
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        adapter.filter(newText);
        return false;
    }
}

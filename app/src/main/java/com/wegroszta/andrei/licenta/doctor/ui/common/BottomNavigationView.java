package com.wegroszta.andrei.licenta.doctor.ui.common;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wegroszta.andrei.licenta.doctor.R;

public class BottomNavigationView extends LinearLayout implements View.OnClickListener {

    private ImageView ivRequests;
    private ImageView ivAlarms;
    private ImageView ivMyPatients;
    private ImageView ivMyProfile;
    private ImageView ivSettings;

    private OnNavigationClickListener onNavigationClickListener;

    public BottomNavigationView(final Context context) {
        this(context, null);
    }

    public BottomNavigationView(final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public BottomNavigationView(final Context context, @Nullable final AttributeSet attrs,
                                final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(context).inflate(R.layout.layout_bottom_navigation_view, this, true);

        ivRequests = findViewById(R.id.iv_requests);
        ivAlarms = findViewById(R.id.iv_alarms);
        ivMyPatients = findViewById(R.id.iv_my_patients);
        ivMyProfile = findViewById(R.id.iv_my_profile);
        ivSettings = findViewById(R.id.iv_settings);

        findViewById(R.id.ll_nav_item_requests).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_alarms).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_my_patients).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_my_profile).setOnClickListener(this);
        findViewById(R.id.ll_nav_item_settings).setOnClickListener(this);
    }

    @Override
    public void onClick(final View view) {
        if (onNavigationClickListener != null) {
            switch (view.getId()) {
                case R.id.ll_nav_item_requests:
                    selectRequests();
                    onNavigationClickListener.onRequestsClicked();
                    break;
                case R.id.ll_nav_item_alarms:
                    selectAlarms();
                    onNavigationClickListener.onAlarmsClicked();
                    break;
                case R.id.ll_nav_item_my_patients:
                    selectMyPatients();
                    onNavigationClickListener.onMyPatientsClicked();
                    break;
                case R.id.ll_nav_item_my_profile:
                    selectMyProfile();
                    onNavigationClickListener.onMyProfileClicked();
                    break;
                case R.id.ll_nav_item_settings:
                    selectSettings();
                    onNavigationClickListener.onSettingsClicked();
                    break;
            }
        }
    }

    private void selectRequests() {
        ivRequests.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_requests));
        ivAlarms.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyPatients.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectAlarms() {
        ivRequests.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivAlarms.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_alarms));
        ivMyPatients.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectMyPatients() {
        ivRequests.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivAlarms.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyPatients.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_my_patients));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectMyProfile() {
        ivRequests.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivAlarms.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyPatients.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_my_profile));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
    }

    private void selectSettings() {
        ivRequests.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivAlarms.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyPatients.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivMyProfile.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_unpressed));
        ivSettings.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_item_settings));
    }

    public void selectPage(final Page page) {
        switch (page) {
            case REQUESTS:
                selectRequests();
                break;
            case ALARMS:
                selectAlarms();
                break;
            case MY_PATIENTS:
                selectMyPatients();
                break;
            case MY_PROFILE:
                selectMyProfile();
                break;
            case SETTINGS:
                selectSettings();
                break;
        }
    }

    public void setOnNavigationClickListener(final OnNavigationClickListener onNavigationClickListener) {
        this.onNavigationClickListener = onNavigationClickListener;
    }

    public interface OnNavigationClickListener {
        void onRequestsClicked();

        void onAlarmsClicked();

        void onMyPatientsClicked();

        void onMyProfileClicked();

        void onSettingsClicked();
    }

    public enum Page {
        REQUESTS,
        ALARMS,
        MY_PATIENTS,
        MY_PROFILE,
        SETTINGS
    }
}
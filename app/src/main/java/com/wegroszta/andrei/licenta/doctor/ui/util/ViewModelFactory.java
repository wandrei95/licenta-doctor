package com.wegroszta.andrei.licenta.doctor.ui.util;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.wegroszta.andrei.licenta.doctor.io.alarms.FirebaseAlarmsStorage;
import com.wegroszta.andrei.licenta.doctor.io.auth.FirebaseAuthenticator;
import com.wegroszta.andrei.licenta.doctor.io.auth.FirebaseUserCreator;
import com.wegroszta.andrei.licenta.doctor.io.auth.FirebaseUserStorage;
import com.wegroszta.andrei.licenta.doctor.io.doctors.FirebaseDoctorsStorage;
import com.wegroszta.andrei.licenta.doctor.io.patientdetails.FirebasePatientStorage;
import com.wegroszta.andrei.licenta.doctor.io.requests.FirebaseAcceptedPatientsStorage;
import com.wegroszta.andrei.licenta.doctor.io.requests.FirebaseCollaborationRequestsStorage;
import com.wegroszta.andrei.licenta.doctor.io.statistics.FirebaseHeartRateStatisticsFetcher;
import com.wegroszta.andrei.licenta.doctor.io.statistics.FirebaseTemperatureStatisticsFetcher;
import com.wegroszta.andrei.licenta.doctor.usecases.alarms.AlarmsInteractor;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.Login;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.doctor.usecases.doctors.DoctorsInteractor;
import com.wegroszta.andrei.licenta.doctor.usecases.patientdetails.PatientInteractor;
import com.wegroszta.andrei.licenta.doctor.usecases.requests.CollaborationRequestsInteractor;
import com.wegroszta.andrei.licenta.doctor.usecases.statistics.heartrate.FetchHeartRateStatistics;
import com.wegroszta.andrei.licenta.doctor.usecases.statistics.temperature.FetchTemperatureStatistics;
import com.wegroszta.andrei.licenta.doctor.viewmodels.alarms.AlarmsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.auth.LoginViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.doctors.DoctorsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.patientdetails.PatientDetailsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.requests.CollaborationRequestsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.statistics.FetchHeartRateStatisticsViewModel;
import com.wegroszta.andrei.licenta.doctor.viewmodels.statistics.FetchTemperatureStatisticsViewModel;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private static final Object lock = new Object();
    private static ViewModelFactory instance;

    private ViewModelFactory() {

    }

    public static ViewModelFactory getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new ViewModelFactory();
                }
            }
        }
        return instance;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull final Class<T> modelClass) {
        if (modelClass == LoginViewModel.class) {
            return (T) createNewLoginViewModel();
        } else if (modelClass == PatientDetailsViewModel.class) {
            return (T) createNewPatientDetailsViewModel();
        } else if (modelClass == CollaborationRequestsViewModel.class) {
            return (T) createNewCollaborationRequestsViewModel();
        } else if (modelClass == FetchHeartRateStatisticsViewModel.class) {
            return (T) createNewFetchHeartRateStatisticsViewModel();
        } else if (modelClass == FetchTemperatureStatisticsViewModel.class) {
            return (T) createNewFetchTemperatureStatisticsViewModel();
        } else if (modelClass == DoctorsViewModel.class) {
            return (T) createNewDoctorsViewModel();
        } else if (modelClass == AlarmsViewModel.class) {
            return (T) createNewAlarmsViewModel();
        }
        return super.create(modelClass);
    }

    @NonNull
    private LoginViewModel createNewLoginViewModel() {
        return new LoginViewModel(new Login(new FirebaseAuthenticator(FirebaseAuth.getInstance()),
                new FirebaseUserCreator(FirebaseAuth.getInstance(), FirebaseDatabase.getInstance().getReference())));
    }

    @NonNull
    private PatientDetailsViewModel createNewPatientDetailsViewModel() {
        return new PatientDetailsViewModel(new PatientInteractor(new FirebasePatientStorage(FirebaseDatabase.getInstance().getReference())));
    }

    @NonNull
    private CollaborationRequestsViewModel createNewCollaborationRequestsViewModel() {
        return new CollaborationRequestsViewModel(
                new CollaborationRequestsInteractor(new FirebaseCollaborationRequestsStorage(FirebaseDatabase.getInstance().getReference()),
                        new FirebaseAcceptedPatientsStorage(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    @NonNull
    private FetchHeartRateStatisticsViewModel createNewFetchHeartRateStatisticsViewModel() {
        return new FetchHeartRateStatisticsViewModel(
                new FetchHeartRateStatistics(new FirebaseHeartRateStatisticsFetcher(FirebaseDatabase.getInstance().getReference())));
    }

    private FetchTemperatureStatisticsViewModel createNewFetchTemperatureStatisticsViewModel() {
        return new FetchTemperatureStatisticsViewModel(
                new FetchTemperatureStatistics(new FirebaseTemperatureStatisticsFetcher(FirebaseDatabase.getInstance().getReference())));
    }

    @NonNull
    private DoctorsViewModel createNewDoctorsViewModel() {
        return new DoctorsViewModel(new DoctorsInteractor(new FirebaseDoctorsStorage(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }

    @NonNull
    private AlarmsViewModel createNewAlarmsViewModel() {
        return new AlarmsViewModel(new AlarmsInteractor(new FirebaseAlarmsStorage(FirebaseDatabase.getInstance().getReference())),
                new UserDataFetcher(new FirebaseUserStorage(FirebaseAuth.getInstance())));
    }
}

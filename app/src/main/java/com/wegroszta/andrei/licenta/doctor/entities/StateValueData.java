package com.wegroszta.andrei.licenta.doctor.entities;

public class StateValueData extends StateData {
    private final double value;

    public StateValueData() {
        this(0L, State.AVG, 0d);
    }

    public StateValueData(long timestamp, double value) {
        this(timestamp, State.AVG, value);
    }

    public StateValueData(long timestamp, State state, double value) {
        super(timestamp, state);
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}


package com.wegroszta.andrei.licenta.doctor.viewmodels.doctors;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.doctor.entities.Doctor;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.doctor.usecases.doctors.DoctorsInteractor;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DoctorsViewModel extends ViewModel {
    private final DoctorsInteractor doctorsInteractor;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<Doctor>> getDoctorResponse;
    private final MutableLiveData<Response<Void>> saveDoctorResponse;
    private final CompositeDisposable disposables;

    public DoctorsViewModel(final DoctorsInteractor doctorsInteractor, final UserDataFetcher userDataFetcher) {
        this.doctorsInteractor = doctorsInteractor;
        this.userDataFetcher = userDataFetcher;
        getDoctorResponse = new MutableLiveData<>();
        saveDoctorResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<Doctor>> getGetDoctorResponse() {
        return getDoctorResponse;
    }

    public MutableLiveData<Response<Void>> getSaveDoctorResponse() {
        return saveDoctorResponse;
    }

    public void saveDoctorDetails(final Doctor doctor) {
        final String userId = userDataFetcher.getLoggedUserId();
        doctor.setId(userId);
        disposables.add(doctorsInteractor.saveDoctor(doctor)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> saveDoctorResponse.setValue(Response.started()))
                .subscribe(
                        aVoid -> saveDoctorResponse.setValue(Response.success(aVoid)),
                        throwable -> saveDoctorResponse.setValue(Response.failure(throwable)),
                        () -> saveDoctorResponse.setValue(Response.completed(null))
                )
        );
    }

    public void getLoggedDoctor() {
        final String userId = userDataFetcher.getLoggedUserId();
        disposables.add(doctorsInteractor.getDoctor(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getDoctorResponse.setValue(Response.started()))
                .subscribe(
                        patient -> getDoctorResponse.setValue(Response.success(patient)),
                        throwable -> getDoctorResponse.setValue(Response.failure(throwable)),
                        () -> getDoctorResponse.setValue(Response.completed(null))
                )
        );
    }
}

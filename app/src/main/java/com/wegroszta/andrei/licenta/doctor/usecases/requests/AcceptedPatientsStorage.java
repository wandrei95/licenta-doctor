package com.wegroszta.andrei.licenta.doctor.usecases.requests;

import com.wegroszta.andrei.licenta.doctor.entities.AcceptedPatient;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;

import java.util.List;

import io.reactivex.Observable;

public interface AcceptedPatientsStorage {
    Observable<CollaborationRequest> saveAcceptedPatient(String doctorId,
                                                         CollaborationRequest collaborationRequest);

    Observable<List<AcceptedPatient>> getPatientsForDoctor(String doctorId);

    Observable<Void> removeAcceptedPatient(String doctorId, String patientId);
}

package com.wegroszta.andrei.licenta.doctor.usecases.patientdetails;

import com.wegroszta.andrei.licenta.doctor.entities.Patient;

import io.reactivex.Observable;

public class PatientInteractor {
    private final PatientStorage patientStorage;

    public PatientInteractor(final PatientStorage patientStorage) {
        this.patientStorage = patientStorage;
    }

    public Observable<Patient> getPatient(final String patientId) {
        return patientStorage.getPatientForId(patientId);
    }
}

package com.wegroszta.andrei.licenta.doctor.viewmodels.requests;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wegroszta.andrei.licenta.doctor.entities.AcceptedPatient;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.UserDataFetcher;
import com.wegroszta.andrei.licenta.doctor.usecases.requests.CollaborationRequestsInteractor;
import com.wegroszta.andrei.licenta.doctor.viewmodels.responses.Response;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CollaborationRequestsViewModel extends ViewModel {
    private final CollaborationRequestsInteractor collaborationRequestsInteractor;
    private final UserDataFetcher userDataFetcher;
    private final MutableLiveData<Response<List<CollaborationRequest>>> getRequestsResponse;
    private final MutableLiveData<Response<CollaborationRequest>> acceptCollaborationRequestResponse;
    private final MutableLiveData<Response<CollaborationRequest>> declineCollaborationRequestResponse;
    private final MutableLiveData<Response<Void>> removePatientResponse;
    //TODO move this to another view model :)
    private final MutableLiveData<Response<List<AcceptedPatient>>> getMyPatientsResponse;
    private final CompositeDisposable disposables;

    public CollaborationRequestsViewModel(final CollaborationRequestsInteractor collaborationRequestsInteractor,
                                          final UserDataFetcher userDataFetcher) {
        this.collaborationRequestsInteractor = collaborationRequestsInteractor;
        this.userDataFetcher = userDataFetcher;
        getRequestsResponse = new MutableLiveData<>();
        acceptCollaborationRequestResponse = new MutableLiveData<>();
        declineCollaborationRequestResponse = new MutableLiveData<>();
        removePatientResponse = new MutableLiveData<>();
        getMyPatientsResponse = new MutableLiveData<>();
        disposables = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public MutableLiveData<Response<List<CollaborationRequest>>> getGetRequestsResponse() {
        return getRequestsResponse;
    }

    public MutableLiveData<Response<CollaborationRequest>> getAcceptCollaborationRequestResponse() {
        return acceptCollaborationRequestResponse;
    }

    public MutableLiveData<Response<CollaborationRequest>> getDeclineCollaborationRequestResponse() {
        return declineCollaborationRequestResponse;
    }
    public MutableLiveData<Response<Void>> getRemovePatientResponse() {
        return removePatientResponse;
    }

    public MutableLiveData<Response<List<AcceptedPatient>>> getGetMyPatientsResponse() {
        return getMyPatientsResponse;
    }


    public void getRequests() {
        final String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(collaborationRequestsInteractor.getRequests(doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getRequestsResponse.setValue(Response.started()))
                .subscribe(
                        requests -> getRequestsResponse.setValue(Response.success(requests)),
                        throwable -> getRequestsResponse.setValue(Response.failure(throwable)),
                        () -> getRequestsResponse.setValue(Response.completed(null))
                )
        );
    }

    public void acceptCollaborationRequest(final CollaborationRequest collaborationRequest) {
        final String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(collaborationRequestsInteractor.acceptRequest(doctorId, collaborationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> acceptCollaborationRequestResponse.setValue(Response.started()))
                .subscribe(
                        request -> acceptCollaborationRequestResponse.setValue(Response.success(request)),
                        throwable -> acceptCollaborationRequestResponse.setValue(Response.failure(throwable)),
                        () -> acceptCollaborationRequestResponse.setValue(Response.completed(null))
                )
        );
    }

    public void getMyPatients() {
        final String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(collaborationRequestsInteractor.getPatientsForDoctor(doctorId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> getMyPatientsResponse.setValue(Response.started()))
                .subscribe(
                        acceptedPatients -> getMyPatientsResponse.setValue(Response.success(acceptedPatients)),
                        throwable -> getMyPatientsResponse.setValue(Response.failure(throwable)),
                        () -> getMyPatientsResponse.setValue(Response.completed(null))
                )
        );
    }

    public void declineCollaborationRequest(final CollaborationRequest collaborationRequest) {
        final String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(collaborationRequestsInteractor.declineRequest(doctorId, collaborationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> declineCollaborationRequestResponse.setValue(Response.started()))
                .subscribe(
                        request -> declineCollaborationRequestResponse.setValue(Response.success(request)),
                        throwable -> declineCollaborationRequestResponse.setValue(Response.failure(throwable)),
                        () -> declineCollaborationRequestResponse.setValue(Response.completed(null))
                )
        );
    }

    public void removePatient(final String patientId) {
        final String doctorId = userDataFetcher.getLoggedUserId();
        disposables.add(collaborationRequestsInteractor.removePatient(doctorId, patientId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> removePatientResponse.setValue(Response.started()))
                .subscribe(
                        request -> removePatientResponse.setValue(Response.success(request)),
                        throwable -> removePatientResponse.setValue(Response.failure(throwable)),
                        () -> removePatientResponse.setValue(Response.completed(null))
                )
        );
    }
}

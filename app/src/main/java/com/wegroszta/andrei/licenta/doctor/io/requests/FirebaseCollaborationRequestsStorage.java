package com.wegroszta.andrei.licenta.doctor.io.requests;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.doctor.entities.CollaborationRequest;
import com.wegroszta.andrei.licenta.doctor.usecases.requests.CollaborationRequestsStorage;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseCollaborationRequestsStorage implements CollaborationRequestsStorage {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseCollaborationRequestsStorage(DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<List<CollaborationRequest>> getRequestsForDoctor(
            final String doctorId) {
        return Observable.create(source -> fetchRequests(doctorId, source));
    }

    private void fetchRequests(final String doctorId,
                               final ObservableEmitter<List<CollaborationRequest>> source) {
        DatabaseReference databaseReference = getCollaborationRequestsDatabaseReference(doctorId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    @NonNull
    private ValueEventListener createValueEventListener(
            final ObservableEmitter<List<CollaborationRequest>> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readRequests(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readRequests(final DataSnapshot dataSnapshot,
                              final ObservableEmitter<List<CollaborationRequest>> source) {
        List<CollaborationRequest> requests = getRequestsFromDataSnapshot(dataSnapshot);
        source.onNext(requests);
        source.onComplete();
    }

    @NonNull
    private List<CollaborationRequest> getRequestsFromDataSnapshot(
            final DataSnapshot dataSnapshot) {
        List<CollaborationRequest> requests = new ArrayList<>();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            CollaborationRequest request = postSnapshot.getValue(CollaborationRequest.class);
            requests.add(request);
        }
        return requests;
    }

    @Override
    public Observable<CollaborationRequest> remove(final String doctorId,
                                                   final CollaborationRequest collaborationRequest) {
        return Observable.create(source -> removeRequest(doctorId, collaborationRequest, source));
    }

    private void removeRequest(final String doctorId, final CollaborationRequest collaborationRequest,
                               final ObservableEmitter<CollaborationRequest> source) {
        DatabaseReference databaseReference = getCollaborationRequestsDatabaseReference(doctorId)
                .child(collaborationRequest.getId());
        databaseReference.removeValue((databaseError, databaseReference1) -> {
            if (databaseError == null) {
                source.onNext(collaborationRequest);
                source.onComplete();
            } else {
                source.onError(databaseError.toException());
            }
        });
    }

    private DatabaseReference getCollaborationRequestsDatabaseReference(final String doctorId) {
        return baseDatabaseReference
                .child("doctors")
                .child(doctorId)
                .child("collaborationRequests");
    }
}

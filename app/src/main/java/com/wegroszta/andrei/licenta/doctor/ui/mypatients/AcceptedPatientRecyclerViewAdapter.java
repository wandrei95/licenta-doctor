package com.wegroszta.andrei.licenta.doctor.ui.mypatients;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wegroszta.andrei.licenta.doctor.R;
import com.wegroszta.andrei.licenta.doctor.entities.AcceptedPatient;

import java.util.ArrayList;
import java.util.List;

public class AcceptedPatientRecyclerViewAdapter extends RecyclerView.Adapter<AcceptedPatientRecyclerViewAdapter.PatientViewHolder> {
    private final List<AcceptedPatient> patients;
    private final List<AcceptedPatient> filteredPatients;
    private OnPatientClickListener onPatientClickListener;

    AcceptedPatientRecyclerViewAdapter() {
        this.patients = new ArrayList<>();
        this.filteredPatients = new ArrayList<>();
    }

    public void setPatients(final List<AcceptedPatient> patients) {
        this.patients.clear();
        this.patients.addAll(patients);
        this.filteredPatients.clear();
        this.filteredPatients.addAll(patients);
        notifyDataSetChanged();
    }

    public void setOnPatientClickListener(final OnPatientClickListener onPatientClickListener) {
        this.onPatientClickListener = onPatientClickListener;
    }

    @NonNull
    @Override
    public PatientViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.patient_layout, parent, false);
        return new PatientViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final PatientViewHolder holder, final int position) {
        AcceptedPatient patient = filteredPatients.get(position);
        String name = patient.getFirstName() + " " + patient.getLastName();

        holder.tvName.setText(name);
        holder.container.setOnClickListener(v -> {
            if (onPatientClickListener != null) {
                onPatientClickListener.onPatientClicked(patient);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredPatients.size();
    }

    public void filter(final String filterText) {
        filteredPatients.clear();

        if (filterText == null || filterText.trim().isEmpty()) {
            filteredPatients.addAll(patients);
            notifyDataSetChanged();
        } else {
            for (AcceptedPatient patient : patients) {
                if (patient.getFirstName().toUpperCase().contains(filterText.toUpperCase())
                        || patient.getLastName().toUpperCase().contains(filterText.toUpperCase())) {
                    filteredPatients.add(patient);
                }
            }
            notifyDataSetChanged();
        }
    }

    class PatientViewHolder extends RecyclerView.ViewHolder {
        final TextView tvName;
        final View container;

        PatientViewHolder(final View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            container = itemView;
        }
    }

    public interface OnPatientClickListener {
        void onPatientClicked(AcceptedPatient acceptedPatient);
    }
}
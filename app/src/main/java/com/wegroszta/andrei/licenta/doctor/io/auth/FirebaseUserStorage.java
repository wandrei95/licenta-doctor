package com.wegroszta.andrei.licenta.doctor.io.auth;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.UserStorage;

public class FirebaseUserStorage implements UserStorage {
    private final FirebaseAuth firebaseAuth;

    public FirebaseUserStorage(final FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public String getLoggedUserId() {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
        if (firebaseUser != null) {
            return firebaseUser.getUid();
        }
        return "";
    }
}

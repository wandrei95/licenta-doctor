package com.wegroszta.andrei.licenta.doctor.ui.util;

import com.jjoe64.graphview.series.DataPoint;
import com.wegroszta.andrei.licenta.doctor.entities.StateValueData;

public class StateValueDataPoint extends DataPoint {
    private final StateValueData data;

    public StateValueDataPoint(final double x, final double y, final StateValueData data) {
        super(x, y);
        this.data = data;
    }

    public StateValueData getData() {
        return data;
    }
}
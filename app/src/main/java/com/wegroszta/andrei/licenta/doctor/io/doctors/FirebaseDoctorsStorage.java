package com.wegroszta.andrei.licenta.doctor.io.doctors;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.doctor.entities.Doctor;
import com.wegroszta.andrei.licenta.doctor.usecases.doctors.DoctorsStorage;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebaseDoctorsStorage implements DoctorsStorage {
    private final DatabaseReference baseDatabaseReference;

    public FirebaseDoctorsStorage(DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Doctor> getDoctor(final String id) {
        return Observable.create(source -> fetchDoctor(id, source));
    }

    @Override
    public Observable<Void> saveDoctor(final Doctor doctor) {
        return Observable.create(source -> saveDoctor(doctor, source));

    }

    private void saveDoctor(final Doctor doctor, final ObservableEmitter<Void> source) {
        getDatabaseReferenceForDoctorDetails(doctor.getId())
                .setValue(doctor)
                .addOnCompleteListener(task -> source.onComplete())
                .addOnFailureListener(source::onError);
    }

    private void fetchDoctor(final String id, final ObservableEmitter<Doctor> source) {
        DatabaseReference databaseReference = getDatabaseReferenceForDoctorDetails(id);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    private ValueEventListener createValueEventListener(final ObservableEmitter<Doctor> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readDoctor(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readDoctor(final DataSnapshot dataSnapshot, final ObservableEmitter<Doctor> source) {
        Doctor doctor = dataSnapshot.getValue(Doctor.class);
        source.onNext(doctor);
        source.onComplete();
    }

    private DatabaseReference getDatabaseReferenceForDoctorDetails(final String id) {
        return baseDatabaseReference
                .child("doctors")
                .child(id)
                .child("details");
    }
}

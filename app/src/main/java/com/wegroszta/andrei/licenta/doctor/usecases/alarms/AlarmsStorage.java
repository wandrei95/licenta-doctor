package com.wegroszta.andrei.licenta.doctor.usecases.alarms;

import com.wegroszta.andrei.licenta.doctor.entities.Alarm;

import java.util.List;

import io.reactivex.Observable;

public interface AlarmsStorage {
    Observable<List<Alarm>> getAlarms(String doctorId);

    Observable<Alarm> removeAlarm(String doctorId, Alarm alarm);
}

package com.wegroszta.andrei.licenta.doctor.io.auth;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.wegroszta.andrei.licenta.doctor.entities.Credentials;
import com.wegroszta.andrei.licenta.doctor.io.SharedPrefUtil;
import com.wegroszta.andrei.licenta.doctor.usecases.auth.Authenticator;

import io.reactivex.Observable;

public class FirebaseAuthenticator implements Authenticator {
    private final FirebaseAuth firebaseAuth;

    public FirebaseAuthenticator(final FirebaseAuth firebaseAuth) {
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public Observable<Void> authenticate(final Credentials credentials) {
        return Observable.create(e -> firebaseAuth.signInWithEmailAndPassword(credentials.getEmail(), credentials.getPassword())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String userId = firebaseAuth.getCurrentUser().getUid();
                        DatabaseReference baseDatabaseReference = FirebaseDatabase.getInstance().getReference();
                        baseDatabaseReference.child("doctors").child(userId).child("fcmToken")
                                .setValue(SharedPrefUtil.getInstance().getFcmToken())
                                .addOnCompleteListener(t -> {
                                    e.onComplete();
                                })
                                .addOnFailureListener(fcmTokenSaveException -> {
                                    e.onError(new RuntimeException("Sign Up Failed"));
                                });
                    } else {
                        e.onError(new RuntimeException("Sign Up Failed"));
                    }
                }));
    }

    @Override
    public Observable<Void> logout() {
        String userId = firebaseAuth.getCurrentUser().getUid();
        return Observable.create(e -> {
            DatabaseReference baseDatabaseReference = FirebaseDatabase.getInstance()
                    .getReference()
                    .child("doctors")
                    .child(userId)
                    .child("fcmToken");
            baseDatabaseReference.removeValue((databaseError, databaseReference1) -> {
                if (databaseError == null) {
                    firebaseAuth.signOut();
                    e.onComplete();
                } else {
                    e.onError(databaseError.toException());
                }
            });
        });
    }

    @Override
    public boolean isAnyUserLogged() {
        return firebaseAuth.getCurrentUser() != null;
    }
}

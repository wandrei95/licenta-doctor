package com.wegroszta.andrei.licenta.doctor.io.patientdetails;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wegroszta.andrei.licenta.doctor.entities.Patient;
import com.wegroszta.andrei.licenta.doctor.usecases.patientdetails.PatientStorage;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

public class FirebasePatientStorage implements PatientStorage {
    private final DatabaseReference baseDatabaseReference;

    public FirebasePatientStorage(DatabaseReference baseDatabaseReference) {
        this.baseDatabaseReference = baseDatabaseReference;
    }

    @Override
    public Observable<Patient> getPatientForId(final String patientId) {
        return Observable.create(source -> fetchPatient(patientId, source));
    }

    private void fetchPatient(final String patientId, final ObservableEmitter<Patient> source) {
        DatabaseReference databaseReference = getDatabaseReference(patientId);
        databaseReference.addListenerForSingleValueEvent(createValueEventListener(source));
    }

    @NonNull
    private ValueEventListener createValueEventListener(final ObservableEmitter<Patient> source) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                readPatient(dataSnapshot, source);
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                source.onError(databaseError.toException());
            }
        };
    }

    private void readPatient(final DataSnapshot dataSnapshot, final ObservableEmitter<Patient> source) {
        Patient patient = dataSnapshot.getValue(Patient.class);
        source.onNext(patient);
        source.onComplete();
    }

    private DatabaseReference getDatabaseReference(final String patientId) {
        return baseDatabaseReference
                .child("patients")
                .child(patientId)
                .child("details");
    }
}

package com.wegroszta.andrei.licenta.doctor.usecases.statistics.heartrate;

import com.wegroszta.andrei.licenta.doctor.entities.HeartRate;

import java.util.List;

import io.reactivex.Observable;

public class FetchHeartRateStatistics {
    private final HeartRateStatisticsFetcher heartRateStatisticsFetcher;

    public FetchHeartRateStatistics(final HeartRateStatisticsFetcher heartRateStatisticsFetcher) {
        this.heartRateStatisticsFetcher = heartRateStatisticsFetcher;
    }

    public Observable<List<HeartRate>> observeHeartRateStatistics(final String patientId) {
        return heartRateStatisticsFetcher.getHeartRate(patientId);
    }
}

package com.wegroszta.andrei.licenta.doctor.usecases.auth;

import com.wegroszta.andrei.licenta.doctor.entities.Credentials;

import io.reactivex.Observable;

public interface Authenticator {
    Observable<Void> authenticate(Credentials credentials);

    Observable<Void> logout();

    boolean isAnyUserLogged();
}

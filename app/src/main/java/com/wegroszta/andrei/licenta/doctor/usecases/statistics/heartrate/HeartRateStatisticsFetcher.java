package com.wegroszta.andrei.licenta.doctor.usecases.statistics.heartrate;

import com.wegroszta.andrei.licenta.doctor.entities.HeartRate;

import java.util.List;

import io.reactivex.Observable;

public interface HeartRateStatisticsFetcher {
    Observable<List<HeartRate>> getHeartRate(String patientId);
}

package com.wegroszta.andrei.licenta.doctor.usecases.statistics.temperature;

import com.wegroszta.andrei.licenta.doctor.entities.Temperature;

import java.util.List;

import io.reactivex.Observable;

public interface TemperatureStatisticsFetcher {
    Observable<List<Temperature>> getTemperatures(String patientId);
}
